---
layout: page
title: "⧉Info⧉"
image:
  feature: info-id.png
comments: false
modified: 2019-05-22T13:15:59-23:00
---

As you can see, I've been busy. That's 8 different websites, all published to the same domain.

What is likly to happen, is there will be a split among domains, and eventually I'll find some CMS that I want to publish a few of the repositories together, with.

* [About Me](https://infominer.id/about)

I know it's better seo value to keep everything on one domain, but I intend to create a lot of content on a variety of subjects, some of which are only tangentally related.

But you can see, eventually this will be 5 domains with infominer.id becoming the home page for all of it, webwork.tools, didecentral.com, SourceCrypto (don't remember if I have one for that), learncryptotrading.co.

I should probably hoook each of those domains up with a re-direct right now... I've just been pre-occupied with other things. That way I could still gain some promotional benefit of using the domain, while retaining the SEO value of having them live together for the time being.

WebWork will be a mix of traditional web-technologies, and practical application of new web-tech such as ipfs and blockchain. It's also where any practical data-science experiments would live.

Ok, thanks for having this chat with me!

## GitHub Repository Portfolio

* [@infominer33's Github Repository Portfolio](https://infominer.id/github-portfolio)

[![](https://imgur.com/NgEKu1x.png)](https://github.com/infominer33)


## Web Work
* [Indieweb](https://infominer.id/indieweb)
* [Web Work](https://infomienr.id/web-work)

## Decentralized Identity

* [DIDecentralized](https://infominer.id/DIDecentralized)

I already have purchased DIDecentral.com, but for now, the SEO value is better to keep tied to infominer.id.

## Source Crypto
* [InfoMine](https://infominer.id)
* [Source Crypto](https://infominer.id/SourceCrypto)
* [Bitcoin History](https://infominer.id/bitcoin-history)
* [Transcripts](https://infomienr.id/transcripts)

## Learn Crypto Trading
* [Learn Crypto Trading](https://infominer.id/learn-crypto-trading)

## Information Dense Content

I'm not a fast writer, or even particularly good at it. However, I love to learn. I value reliable sources and am a patient editor. 

When working with a client, especially new clients, I ask plenty of questions, and show my progress, to ensure that I'm headed in the right direction.

I'm primarily interested in Webwork, Bitcoin, *decentralized identity*, cryptocurrencies, development, and tech trends. 

I'm also quite interested in [data science](https://www.csbtechemporium.com/deep-learning-revolution/), although not nearly as much as I am crypto.

**I aim to create content of the highest quality - relating complex subjects in a clear and concise fashion - supplying essential details, with quality sources for further study.**
{: .notice--warning}
