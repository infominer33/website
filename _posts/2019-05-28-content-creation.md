---
layout: post
title: "Resources for Content Creation"
description: "All kinda tools for images and editing and handy stuff to assist with content creation."
modified: 2019-05-29T13:33:33-23:00
tags: 
  - content creation
  - tools
  - resources
permalink: content-creation/

image:
  feature: content-creation.png
  thumb: /images/content-creation.png
  og_image: content-thumb.png

---

Here's some tools to make content creation a little easier.

As per usual, this list will grow and become more organized over time.

For now, it's just a copy\paste from the [GitHub-Pages Starter Pack].

## Basics

* <a href="https://guides.github.com/features/mastering-markdown/" target="_blank">Mastering Markdown</a>
* <a href="https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet" target="_blank">Markdown Cheet-Sheet</a>


## Editors

* [Visual Studio Code](https://code.visualstudio.com/)
Where I build webpages.
  ![](https://imgur.com/eqWiJ8Il.png)]

This thing has extensions for all your coding needs... It is cross-platform, has seamless git integration, and all kind of great features for working with gh-pages repositories (search and replace, etc.).

**HackMD - Collaborative Markdown Editor**
* [https://hackmd.io](https://hackmd.io) - Collaborative markdown editor (where I write everything else).
  * [Getting started](https://hackmd.io/getting-started)  
  * [Tutorial](https://hackmd.io/c/tutorials/%2Fs%2Ftutorials)
  * [How to Create a Book](https://hackmd.io/c/tutorials/%2Fs%2Fhow-to-create-book)
  * [YAML Metadata](https://hackmd.io/c/tutorials-tw/%2Fs%2Fyaml-metadata)
* See also [demo.codimd.org/](https://demo.codimd.org/)

## Public Domain - Stock Images

* [nacyot/awesome-opensource-documents](https://github.com/nacyot/awesome-opensource-documents)
* [neutraltone/awesome-stock-resources](https://github.com/neutraltone/awesome-stock-resources)
* [shime/creative-commons-media](https://github.com/shime/creative-commons-media)
* [idleberg/Creative-Commons-Markdown](https://github.com/idleberg/Creative-Commons-Markdown) - snippets to add CC license to your work.


## Image Sizes for Popular Platforms
* [Twitter-Image Sizes](https://louisem.com/217438/twitter-image-size)


## Images - Infographics

* [Canva Infographic Creator](https://www.canva.com/create/infographics/)
* [easel.ly](http://www.easel.ly/) - free create infographics
* [Content Strategy Tool](https://builtvisible.com/content-strategy-helper/) - Find inspiration for your content marketing topics 
* [Google Public Data](http://www.google.com/publicdata/directory) - content research, infographics, and more.
* [Google SERP Snippet Optimization Tool](http://www.seomofo.com/snippet-optimizer.html) - see how your snippet may appear in search results. 
* [infogr.am](https://infogr.am/) - create infographics and data visualizations
* [https://www341.lunapic.com/editor/](https://www341.lunapic.com/editor/)
* [Piktochart](http://piktochart.com/) - visualization generator.

### Optimization

The web *loooves* big old beautiful high quality images... However, my visitors probably notice the load time of huge images more than I do (though I do notice them).

* [https://trimage.org/](https://trimage.org/)
  >A cross-platform tool for losslessly optimizing PNG and JPG files for web.
* [pngquant](https://pngquant.org/)
  > is a command-line utility and a library for lossy compression of PNG images.
  >
  >The conversion reduces file sizes significantly (often as much as 70%) and preserves full alpha transparency. Generated images are compatible with all web browsers and operating systems.
* [https://ezgif.com/optimize](https://ezgif.com/optimize)

## Text 
* [Text Cleaner](http://www.textcleanr.com/) - cleans up all kinds of text formatting when copying and pasting between applications.
* [wordle](http://www.wordle.net/) - word cloud generator
* [Yahoo Pipes](http://pipes.yahoo.com/pipes/)
combines feeds "into content and other magical creations". 
* [https://www.pcjs.org](https://www.pcjs.org) - IBM PC simulation that runs in your web browser

## Video
* [Amara](http://amara.org/en/) - create captions for YouTube videos.
* [Wistia](http://wistia.com/) - SEO-friendly video hosting. 

## Plagiarism

* [Copyscape](http://www.copyscape.com/) - track if your content is being plagiarized.

^^^ This is the industry standard. It can help to prevent even plagiarizing from yourself!


## Archiving

* [Web Archive for WayBack Machine](https://chrome.google.com/webstore/detail/web-archive-for-wayback-m/ppokigfjbmhncgkabghdgpiafjdpllke)
* [iipc/awesome-web-archiving](https://github.com/iipc/awesome-web-archiving)
* [webapps.stackexchange.com - how-to-archive-the-whole-website](https://webapps.stackexchange.com/questions/115369/how-to-archive-the-whole-website)
* [httrack.com](http://www.httrack.com)

## Data Visualization

* [rendering-data-as-graphs](https://developer.github.com/v3/guides/rendering-data-as-graphs/)
* [Creating a dynamic d3 visualization from the GitHub API](https://www.benlcollins.com/javascript/creating-a-dynamic-d3-visualization-from-the-github-api/)
* [Visualize GitHub Code Contribution using APP Link](https://www.targetprocess.com/blog/visualize-github-code-contribution-using-app-link/)
* [Data Visualization for All - Modify and Host Code with GitHub](https://datavizforall.org/github.html) by Jack Dougherty & Ilya Ilyankou
  > In the first half of this book, we explored free web services that offer easy drag-and-drop tools to create interactive charts and maps, such as Google Sheets, Google My Maps, BatchGeo, Carto, and Tableau Public. But these web services have limited options for designing and customizing your visualizations, and also make you dependent on their web servers to host your work. In this second half of the book, we’ll explore how to copy, edit, and host code templates, meaning pre-written software instructions to create visualizations. With templates, no prior coding skills are necessary. You will learn how to make simple edits to insert your data, customize its appearance, and display it on the web on a site you control.
* [tools-visualize-github-profile/](https://livablesoftware.com/tools-visualize-github-profile/)


## Tutes \ Walkthrus

* [How to Create an Open-Source Directory on GitHub Pages](https://webdesign.tutsplus.com/tutorials/how-to-create-an-open-source-directory-on-github-pages--cms-26225)
* [What You Can Do With Gists on Github?](https://www.labnol.org/internet/github-gist-tutorial/28499/)

## Assorted

* [https://polyglot.untra.io/](https://polyglot.untra.io/) - multi-lingual publishing.
* [https://konpa.github.io/devicon/](https://konpa.github.io/devicon/)
* [Color Tool](https://material.io/tools/color/#!/?view.left=0&view.right=0&primary.color=455A64)
