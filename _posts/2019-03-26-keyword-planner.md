---
layout: post
title: How to Sign Up and Get Started using Google Adwords Keyword Planner
description: This step-by-step guide will ensure that anyone can try out keyword planner and begin learning a keyword research\SEO fundamental.
modified: 2019-05-23T13:15:59-23:00
tags: [keyword-research, seo, walkthrough, stub, adwords, keyword planner]
image:
  thumb: /images/getting-started-adwords-keyword-planner.jpg
  feature: getting-started-adwords-keyword-planner.jpg
permalink: getting-started-adwords-keyword-planner/


---

**Note:** This is just a "stub", and doesn't go fully into the details of keyword research. I have written an [article on the subject](https://www.csbtechemporium.com/keyword-research-fundamentals/), and begun some study of Keyword Research and SEO, although I'm not by any means an expert. 

I made this, because it was a little confusing for me when I first tried to use it, and I wanted to be sure that eveyone in my organization had a fundamental understanding of keyword research, so they could assist in coming up with seed-keywords, and understand the building blocks of keyword-research. 

Everything builds from this. In my opinion, it will be valuable to get input from everyone in the organization for at least the first round of keyword research, since this is all about the identity of your website and your organization.

This step-by-step guide will ensure that anyone can try out keyword planner and begin learning a keyword research\SEO fundamental. It's a valuable tool to have in your arsenal, but takes some manual effort to get the most out of.

You don't need to worry about it immediately, however, you'll find that many guides have adwords keyword planner as one of the tools to use. This is the really basic instructions for getting started, so anyone should be able to!

## Contents

* [Set up Free Google Ads Account](#set-up-free-google-ads-account)
* [Start Using Keyword Planner](#start-using-keyword-planner-)
* [Download and Explore Data](#download-and-explore-data-)

## Set up Free Google Ads Account

![](https://i.imgur.com/F2FyM0A.png)

go to [ads.google.com](https://ads.google.com) and click start now at the top right corner and choose a gmail account to use.

![](https://i.imgur.com/n0AsI6I.png)

Click "Experienced with Google Ads" at the bottom.

![](https://i.imgur.com/ST8jiVY.png)

Click create an account without a campaign.

![](https://i.imgur.com/FDzdZOo.png)

Click Submit.

OK! Now you have an account w google ads.

![](https://i.imgur.com/MLu3Nx5.png)

You have to click "explore your account" to proceed.

## Start Using Keyword Planner


![](https://i.imgur.com/BPGKUAu.png)


In the top right corner, click the little wrench for tools, and then select "Keyword Planner" on the left of the screen.

![](https://i.imgur.com/9HBzgoa.png)

Click Find New Keywords and then you're good to go!

We're going to enter 10 Seed Keywords, those are different ways someone might search for your business, or a business like it. If you don't get enough keyword ideas then leave the web-site blank.

![](https://i.imgur.com/gYfaH1e.png)

### Download and Explore Data

![](https://i.imgur.com/VC9Mc7x.png)

Google comes up with keywords, based on your seed keywords. Ideally you want google to come up with a lot of them. We'll get to that later.

Go to the top right corner and download the keywords and keyword planner will give you a csv file that we're going to open up in a spreadsheet program.

![](https://i.imgur.com/acoH1WP.png)

You can see that there are a lot of ways to explore this data, I'm going to sort it by volume.

Then I can cut out all of the keywords that are really high volume or by whatever metrics I'm looking at, and use the keywords I get from keyword planner with a number of other tools.


