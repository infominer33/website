# Awesome Freelance Web-Work Resources


So... I was moving this over to [infominer33.github.io/web-work](https://infominer.id/web-work/), but then decided it would be nice to keep all web-working resources in their own repository, that will make theme easier to access\fork, etc.. for those who aren't interested in all the crypto, and probably improve discovery.

also, I'll be able to play around with a few different blog-themes... and see what I like, without disturbing the rest of the site.

Also, I'm interested to collaborate with some other writers, and maybe folk will be more interested to get involved if these resources live independently from my home-page\site...

I've realized that for what I want to do, it make sense to branch out and create new repositories as often as necessary... to the end user there is no difference, but for trying to juggle the vast wealth of knowledge available online.. we are required to modularize.
